ARG VERSION=latest

FROM alpine:${VERSION}

COPY install.sh /root/
RUN /bin/sh /root/install.sh
